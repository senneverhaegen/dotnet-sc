using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using Moq;
using SC.BL;
using SC.BL.Domain;
using SC.DAL;
using Xunit;

namespace Tests
{
  public class TicketManagerTests
  {
    private readonly ITicketManager _mgr;
    private readonly Mock<ITicketRepository> _mockTicketRepo;


    // Gets called before each test
    public TicketManagerTests()
    {
      _mockTicketRepo = new Mock<ITicketRepository>();
      _mgr = new TicketManager(_mockTicketRepo.Object);
    }

    [Fact]
    public void Test_GetTickets_Success()
    {
      // Arrange
      List<Ticket> expected = new List<Ticket>
      {
        new Ticket {TicketNumber = 2, Text = "a"},
        new Ticket {TicketNumber = 3, Text = "a"},
        new Ticket {TicketNumber = 4, Text = "a"}
      };
      _mockTicketRepo.Setup(x => x.ReadTickets()).Returns(expected);

      // Act
      IEnumerable<Ticket> result = _mgr.GetTickets();

      // Assert
//            Assert.Equal(expected, result);
      Assert.Equal(3, result.Count());
    }

    [Fact]
    public void Test_GetTicket_Success()
    {
      // Arrange
      int ticketId = 1;
      Ticket t1 = new Ticket {TicketNumber = 1, Text = "a"};
      _mockTicketRepo.Setup(x => x.ReadTicket(1)).Returns(t1);

      // Act
      Ticket ticket = _mgr.GetTicket(ticketId);

      // Assert
      Assert.Equal(t1, ticket);
    }

    [Fact]
    public void Test_GetTicket_Fail()
    {
      // Arrange
      Ticket t1 = new Ticket {TicketNumber = 1, Text = "a"};
      int ticketId = 2;

      // Act
      Ticket ticket = _mgr.GetTicket(ticketId);

      // Assert
      Assert.NotEqual(t1, ticket);
    }


    [Fact]
    public void Test_AddTicket_AccId_Question_Success()
    {
      // Arrange
      int accId = 55;
      string question = "Why doesn't it work?";
      Ticket t1 = new Ticket()
      {
        TicketNumber = 1,
        AccountId = accId,
        Text = question,
        DateOpened = DateTime.Now,
        State = TicketState.Open
      };

      // Act
      _mgr.AddTicket(accId, question);
      _mockTicketRepo.Setup(x => x.CreateTicket(t1)).Returns(t1);
      _mockTicketRepo.Setup(x => x.ReadTicket(1)).Returns(t1);
      Ticket ticket = _mgr.GetTicket(1);

      // Assert
      Assert.Equal(t1, ticket);
    }

    [Fact]
    public void Test_AddTicket_AccId_Question_Fail()
    {
      // Arrange
      int accId = 55;
      string question =
        "eu turpis egestas pretium aenean pharetra magna ac placerat vestibulum lectus mauris ultrices eros in cursus turpis massa tincidunt dui ut ornare lectus sit amet est placerat in egestas erat imperdiet sed euismod nisi porta lorem mollis aliquam ut porttitor leo a diam sollicitudin tempor id eu nisl nunc mi";
      bool ok = true;
      Ticket t1 = new Ticket()
      {
        TicketNumber = 1,
        AccountId = accId,
        Text = question,
        DateOpened = DateTime.Now,
        State = TicketState.Open
      };

      // Act
      _mockTicketRepo.Setup(x => x.CreateTicket(t1)).Returns(t1);

      try
      {
        _mgr.AddTicket(accId, question);
      }
      catch (ValidationException e)
      {
        ok = false;
      }

      // Assert
      Assert.False(ok);
    }

    [Fact]
    public void Test_AddTicket_AccId_Device_Problem_Success()
    {
      // Arrange
      int accId = 1;
      string problem = "Iphone no work.";
      string device = "iPhone";

      Ticket t1 = new HardwareTicket
      {
        TicketNumber = 1,
        AccountId = accId,
        Text = problem,
        DateOpened = DateTime.Now,
        State = TicketState.Open,
        DeviceName = device
      };

      _mockTicketRepo.Setup(x => x.CreateTicket(t1)).Returns(t1);
      _mockTicketRepo.Setup(x => x.ReadTicket(1)).Returns(t1);

      // Act
      _mgr.AddTicket(accId, device, problem);
      Ticket ticket = _mgr.GetTicket(1);

      // Assert
      Assert.Equal(t1, ticket);
    }

    [Fact]
    public void Test_AddTicket_AccId_Device_Problem_Fail()
    {
      // Arrange
      int accId = 1;
      string problem =
        "eu turpis egestas pretium aenean pharetra magna ac placerat vestibulum lectus mauris ultrices eros in cursus turpis massa tincidunt dui ut ornare lectus sit amet est placerat in egestas erat imperdiet sed euismod nisi porta lorem mollis aliquam ut porttitor leo a diam sollicitudin tempor id eu nisl nunc mi";
      string device = "iPhone";
      bool ok = true;

      Ticket t1 = new HardwareTicket
      {
        TicketNumber = 1,
        AccountId = accId,
        Text = problem,
        DateOpened = DateTime.Now,
        State = TicketState.Open,
        DeviceName = device
      };

      _mockTicketRepo.Setup(x => x.CreateTicket(t1)).Returns(t1);

      // Act

      try
      {
        _mgr.AddTicket(accId, device, problem);
      }
      catch (ValidationException e)
      {
        ok = false;
      }

      // Assert
      Assert.False(ok);
    }


    [Fact]
    public void Test_ChangeTicket_Success()
    {
      // Arrange
      int accId = 56;
      string problem = "Iphone no work.";
      string problem2 = "today no good";

      Ticket t1 = new Ticket()
      {
        TicketNumber = 1,
        AccountId = accId,
        Text = problem,
        DateOpened = DateTime.Now,
        State = TicketState.Open
      };

      // Act
      _mockTicketRepo.Setup(x => x.CreateTicket(t1)).Returns(t1);
      _mockTicketRepo.Setup(x => x.ReadTicket(1)).Returns(t1);
      _mgr.AddTicket(accId, problem);
      Ticket ticket = _mgr.GetTicket(1);
      ticket.Text = problem2;
      _mgr.ChangeTicket(ticket);

      // Assert
      Assert.Equal(ticket.Text, problem2);
    }

    [Fact]
    public void Test_ChangeTicket_Fail()
    {
      // Arrange
      int accId = 56;
      string problem = "Iphone no work.";
      string problem2 =
        "eu turpis egestas pretium aenean pharetra magna ac placerat vestibulum lectus mauris ultrices eros in cursus turpis massa tincidunt dui ut ornare lectus sit amet est placerat in egestas erat imperdiet sed euismod nisi porta lorem mollis aliquam ut porttitor leo a diam sollicitudin tempor id eu nisl nunc mi";
      bool ok = true;
      Ticket t1 = new Ticket()
      {
        TicketNumber = 1,
        AccountId = accId,
        Text = problem,
        DateOpened = DateTime.Now,
        State = TicketState.Open
      };

      // Act
      _mockTicketRepo.Setup(x => x.CreateTicket(t1)).Returns(t1);
      _mockTicketRepo.Setup(x => x.ReadTicket(1)).Returns(t1);
      _mgr.AddTicket(accId, problem);
      Ticket ticket = _mgr.GetTicket(1);
      ticket.Text = problem2;

      try
      {
        _mgr.ChangeTicket(ticket);
      }
      catch (ValidationException e)
      {
        ok = false;
      }

      // Assert
      Assert.False(ok);
    }

    [Fact]
    public void Test_RemoveTicket_Success()
    {
      // Arrange
      int accId = 56;
      string problem = "Iphone no work.";

      Ticket t1 = new Ticket()
      {
        TicketNumber = 1,
        AccountId = accId,
        Text = problem,
        DateOpened = DateTime.Now,
        State = TicketState.Open
      };


      // Act
      _mockTicketRepo.Setup(x => x.CreateTicket(t1)).Returns(t1);
      _mockTicketRepo.Setup(x => x.ReadTicket(1));

      _mgr.AddTicket(accId, problem);
      _mgr.RemoveTicket(1);
      _mockTicketRepo.Setup(x => x.DeleteTicket(1));

      Ticket toCheck = _mgr.GetTicket(1);

      // Assert
      Assert.Null(toCheck);
    }

    [Fact]
    public void Test_RemoveTicket_Fail()
    {
      // Arrange
      int accId = 56;
      string problem =
        "eu turpis egestas pretium aenean pharetra magna ac placerat vestibulum lectus mauris ultrices eros in cursus turpis massa tincidunt dui ut ornare lectus sit amet est placerat in egestas erat imperdiet sed euismod nisi porta lorem mollis aliquam ut porttitor leo a diam sollicitudin tempor id eu nisl nunc mi";
      bool ok = true;
      bool ok2 = true;
      Ticket t1 = new Ticket()
      {
        TicketNumber = 1,
        AccountId = accId,
        Text = problem,
        DateOpened = DateTime.Now,
        State = TicketState.Open
      };


      // Act
      _mockTicketRepo.Setup(x => x.CreateTicket(t1));

      try
      {
        _mgr.AddTicket(accId, problem);
      }
      catch (ValidationException e)
      {
        ok2 = false;
      }

      _mockTicketRepo.Setup(x => x.DeleteTicket(1));

      if (!ok2)
      {
        Ticket ticket = _mgr.GetTicket(1);

        if (ticket != null)
        {
          _mgr.RemoveTicket(1);
        }
        else
        {
          ok = false;
        }
      }

      // Assert
      Assert.False(ok);
    }


    [Fact]
    public void Test_AddTicketResponse_Success()
    {
      // Arrange
      int accId = 56;
      string problem = "Iphone no work.";
      string response = "You no install correct";

      Ticket t1 = new Ticket()
      {
        TicketNumber = 1,
        AccountId = accId,
        Text = problem,
        DateOpened = DateTime.Now,
        State = TicketState.Open
      };

      List<TicketResponse> expected = new List<TicketResponse>
      {
        new TicketResponse
        {
          Id = 1,
          Date = DateTime.Now,
          IsClientResponse = false,
          Text = response,
          Ticket = t1
        }
      };

      // Act
      _mockTicketRepo.Setup(x => x.CreateTicket(t1)).Returns(t1);
      _mockTicketRepo.Setup(x => x.ReadTicket(1)).Returns(t1);
      _mockTicketRepo.Setup(x => x.ReadTicketResponsesOfTicket(1)).Returns(expected);
      _mgr.AddTicket(accId, problem);
      _mgr.AddTicketResponse(1, response, false);
      IEnumerable<TicketResponse> resp = _mgr.GetTicketResponses(1);

      // Assert
      Assert.NotNull(resp);
    }

    [Fact]
    public void Test_AddTicketResponse_Fail()
    {
      // Arrange
      int accId = 56;
      string problem = "Iphone no work.";
      string response = "";
      bool ok = true;
      Ticket t1 = new Ticket()
      {
        TicketNumber = 1,
        AccountId = accId,
        Text = problem,
        DateOpened = DateTime.Now,
        State = TicketState.Open
      };

      List<TicketResponse> expected = new List<TicketResponse>
      {
        new TicketResponse
        {
          Id = 1,
          Date = DateTime.Now,
          IsClientResponse = false,
          Text = response,
          Ticket = t1
        }
      };

      // Act
      _mockTicketRepo.Setup(x => x.CreateTicket(t1)).Returns(t1);
      _mockTicketRepo.Setup(x => x.ReadTicket(1)).Returns(t1);
      _mockTicketRepo.Setup(x => x.ReadTicketResponsesOfTicket(1)).Returns(expected);
      _mgr.AddTicket(accId, problem);
      try
      {
        _mgr.AddTicketResponse(1, response, false);
      }
      catch (ValidationException e)
      {
        ok = false;
      }

      // Assert
      Assert.False(ok);
    }

    [Fact]
    public void Test_GetTicketResponse_Success()
    {
      // Arrange
      int accId = 56;
      string problem = "Iphone no work.";
      string response = "You no install correct";

      Ticket t1 = new Ticket()
      {
        TicketNumber = 1,
        AccountId = accId,
        Text = problem,
        DateOpened = DateTime.Now,
        State = TicketState.Open
      };

      List<TicketResponse> expected = new List<TicketResponse>
      {
        new TicketResponse
        {
          Id = 1,
          Date = DateTime.Now,
          IsClientResponse = false,
          Text = response,
          Ticket = t1
        }
      };

      // Act
      _mockTicketRepo.Setup(x => x.CreateTicket(t1)).Returns(t1);
      _mockTicketRepo.Setup(x => x.ReadTicket(1)).Returns(t1);
      _mockTicketRepo.Setup(x => x.ReadTicketResponsesOfTicket(1)).Returns(expected);
      _mgr.AddTicket(accId, problem);
      Ticket ticket = _mgr.GetTicket(1);
      int ticketNmr = ticket.TicketNumber;
      _mgr.AddTicketResponse(ticketNmr, response, false);
      TicketResponse resp = _mgr.GetTicketResponses(ticketNmr).First(t => t.Text.Equals(response));

      // Assert
      Assert.Equal(resp.Text, response);
    }

  }
}