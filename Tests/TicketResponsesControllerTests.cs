using System;
using System.Collections;
using System.Collections.Generic;
using Microsoft.AspNetCore.Mvc;
using Moq;
using SC.BL;
using SC.BL.Domain;
using SC.UI.Web.MVC.Controllers.Api;
using SC.UI.Web.MVC.Models;
using Xunit;

namespace Tests
{
    public class TicketResponsesControllerTests
    {
        private readonly Mock<ITicketManager> _mgrMock;
        private readonly TicketResponsesController _controller;

        public TicketResponsesControllerTests()
        {
            _mgrMock = new Mock<ITicketManager>();
            _controller = new TicketResponsesController(_mgrMock.Object);
        }

        [Fact]
        public void Get_Returns_Ok()
        {
            // Arrange
            Ticket t1 = new Ticket {TicketNumber = 1, Text = "a"};
            var r1 = new TicketResponse {Id = 1, Text = "a", Ticket = t1};
            var r2 = new TicketResponse {Id = 2, Text = "b", Ticket = t1};
            var responses = new List<TicketResponse>{r1,r2};
            _mgrMock.Setup(x => x.GetTicketResponses(1)).Returns(responses);
            
            // Act
            var actual = _controller.Get(1);
            
            // Assert
            Assert.Equal(typeof(OkObjectResult), actual.GetType());
        }

        [Fact]
        public void Get_Empty_List_Returns_NoContent()
        {
            // Arrange
            var expected = typeof(NoContentResult);
            _mgrMock.Setup(x => x.GetTicketResponses(1)).Returns(new List<TicketResponse>());
            
            // Act
            var actual = _controller.Get(1);
            
            // Assert
            Assert.Equal(expected, actual.GetType());
        }
        [Fact]
        public void Get_Null_Returns_NoContent()
        {
            // Arrange
            var expected = typeof(NoContentResult);
            _mgrMock.Setup(x => x.GetTicketResponses(1)).Returns((IEnumerable<TicketResponse>)null);
            
            // Act
            var actual = _controller.Get(1);
            
            // Assert
            Assert.Equal(expected, actual.GetType());
        }

        [Fact]
        public void Post_Returns_CreatedAtAction()
        {
            Ticket ticket = new Ticket {TicketNumber = 1, Text = "a"};
            NewTicketResponseDTO dto = new NewTicketResponseDTO() {TicketNumber = 1, ResponseText = "a", IsClientResponse = true}; 
            TicketResponse ticketResponse = new TicketResponse
                {Id = dto.TicketNumber, Text = dto.ResponseText, IsClientResponse = dto.IsClientResponse, Date = DateTime.Now, Ticket = ticket};
            
            
            // Arrange
            var expected = typeof(CreatedAtActionResult);
            _mgrMock.Setup(x => x.AddTicketResponse(dto.TicketNumber,dto.ResponseText,dto.IsClientResponse)).Returns(ticketResponse);
            
            // Act
            var actual = _controller.Post(dto);
            
            // Assert
            Assert.Equal(expected, actual.GetType());
        }
    }
}