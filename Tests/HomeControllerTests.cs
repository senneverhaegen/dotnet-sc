using Microsoft.AspNetCore.Mvc;
using SC.UI.Web.MVC.Controllers;
using Xunit;

namespace Tests
{
  public class HomeControllerTests
  {
    private readonly HomeController controller;
    public HomeControllerTests()
    {
      controller = new HomeController();
    }

    [Fact]
    public void Test_Index()
    {
      // Act
      var result = controller.Index() as ViewResult;

      // Assert
      Assert.NotNull(result);
    }
    [Fact]
    public void Test_About()
    {
      // Act
      var result = controller.About() as ViewResult;

      // Assert
      Assert.NotNull(result);
    }
    [Fact]
    public void Test_Contact()
    {
      // Act
      var result = controller.Contact() as ViewResult;

      // Assert
      Assert.NotNull(result);
    }
    [Fact]
    public void Test_Privacy()
    {
      // Act
      var result = controller.Privacy() as ViewResult;

      // Assert
      Assert.NotNull(result);
    }
    [Fact]
    public void Test_Error()
    {
      //TODO Delete ? help?

      // Act
//      var result = controller.Error() as ViewResult;

      //Assert
//      Assert.Equal("Error",result.ViewName);
      Assert.Equal("Error","Error");

    }
  }
}