using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using SC.BL.Domain;
using Xunit;

namespace Tests
{
    public class DomainTests
    {
        #region Ticket

        [Fact]
        public void Ticket_Text_Required()
        {
            // Arrange
            Ticket ticket = new Ticket();
            string expectedErrorMessage = "Text is required";

            // Act
            bool failed = ValidateModel(ticket).Any(t => t.ErrorMessage.Equals(expectedErrorMessage));

            // Assert
            Assert.True(failed);
        }

        [Fact]
        public void Ticket_Text_LengthGreaterThan100_ShouldFail()
        {
            // Arrange
            Ticket ticket = new Ticket();
            ticket.Text = new string('a', 101);
            string expectedErrorMessage = "Er zijn maximaal 100 tekens toegestaan";

            // Act
            bool failed = ValidateModel(ticket).Any(t => t.ErrorMessage.Equals(expectedErrorMessage));

            // Assert
            Assert.True(failed);
        }

        [Fact]
        public void Ticket_Text_LengthMax100_Success()
        {
            // Arrange
            Ticket ticket = new Ticket();
            ticket.Text = "a";
            string expectedErrorMessage = "Er zijn maximaal 100 tekens toegestaan";

            // Act
            bool failed = ValidateModel(ticket).Any(t => t.ErrorMessage.Equals(expectedErrorMessage));

            // Assert
            Assert.False(failed);
        }

        #endregion

        #region HardwareTicket

        [Theory]
        [InlineData("pc-123")]
        [InlineData("abc--")]
        [InlineData("PC-abc")]
        [InlineData("PC-1a")]
        public void HardwareTicket_DeviceNameRegex_ShouldFail(string deviceName)
        {
            // Arrange
            HardwareTicket ticket = new HardwareTicket();
            ticket.DeviceName = deviceName;
            string expectedErrorMessage = "DeviceName does not match regex";

            // Act
            bool failed = ValidateModel(ticket).Any(t => t.ErrorMessage.Equals(expectedErrorMessage));

            // Assert
            Assert.True(failed);
        }

        [Theory]
        [InlineData("PCddaz123")]
        [InlineData("PC123456")]
        [InlineData("ABCSD")]
        public void HardwareTicket_DeviceNameRegex_ShouldSucceed(string deviceName)
        {
            // Arrange
            HardwareTicket ticket = new HardwareTicket();
            ticket.DeviceName = deviceName;
            string expectedErrorMessage = "DeviceName does not match regex";

            // Act
            bool failed = ValidateModel(ticket).Any(t => t.ErrorMessage.Equals(expectedErrorMessage));

            // Assert
            Assert.False(failed);
        }
        #endregion

        #region TicketRespone


        [Fact]
        public void TicketResponse_Text_Required()
        {
            // Arrange
            TicketResponse ticketResponse = new TicketResponse();
            string expectedErrorMessage = "Text is required";

            // Act
            bool failed = ValidateModel(ticketResponse).Any(t => t.ErrorMessage.Equals(expectedErrorMessage));

            // Assert
            Assert.True(failed);
        }
        #endregion

        private IList<ValidationResult> ValidateModel(object model)
        {
            var validationResults = new List<ValidationResult>();
            var ctx = new ValidationContext(model);
            Validator.TryValidateObject(model, ctx, validationResults, true);
            return validationResults;
        }
    }
}