using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.EntityFrameworkCore;
using Moq;
using SC.BL.Domain;
using SC.DAL;
using SC.DAL.EF;
using Xunit;

namespace Tests
{
    public class TicketRepositoryTests
    {
        private readonly bool _useTestConstructor = true;
        private readonly Mock<SupportCenterDbContext> _mockContext;
        private readonly ITicketRepository _repo;

        public TicketRepositoryTests()
        {
            _mockContext = new Mock<SupportCenterDbContext>(_useTestConstructor);
            _repo = new TicketRepository(_mockContext.Object);
        }

        [Fact]
        public void CreateTicket_saves_a_ticket_via_context()
        {
            // Arrange
            var mockSet = new Mock<DbSet<Ticket>>();
            _mockContext.Setup(m => m.Tickets).Returns(mockSet.Object);
            Ticket t1 = new Ticket {TicketNumber = 1, Text = "a"};

            // Act
            _repo.CreateTicket(t1);


            // Assert
            mockSet.Verify(m => m.Add(It.IsAny<Ticket>()), Times.Once());
            _mockContext.Verify(m => m.SaveChanges(), Times.Once());
        }

        [Fact]
        public void ReadTicketsTest()
        {
            Ticket t1 = new Ticket {TicketNumber = 1, Text = "a"};
            Ticket t2 = new Ticket {TicketNumber = 2, Text = "a"};
            Ticket t3 = new Ticket {TicketNumber = 3, Text = "a"};
            var data = new List<Ticket> {t1, t2, t3};

            var mockSet = new Mock<DbSet<Ticket>>();
            mockSet.As<IQueryable<Ticket>>().Setup(m => m.GetEnumerator()).Returns(data.GetEnumerator());

            _mockContext.Setup(m => m.Tickets).Returns(mockSet.Object);

            var repo = new TicketRepository(_mockContext.Object);
            var tickets = repo.ReadTickets().ToList();

            Assert.Equal(t1, tickets[0]);
            Assert.Equal(t2, tickets[1]);
            Assert.Equal(t3, tickets[2]);
            Assert.Equal(3, tickets.Count);
        }

        [Fact]
        public void ReadTicketsByTextTest()
        {
            string s1 = "inlog";
            string s2 = "inloggen";
            string s3 = "test";
            Ticket t1 = new Ticket {TicketNumber = 1, Text = "inloggen"};
            var data = new List<Ticket> {t1}.AsQueryable();

            var mockSet = new Mock<DbSet<Ticket>>();
            mockSet.As<IQueryable<Ticket>>().Setup(m => m.Provider).Returns(data.Provider);
            mockSet.As<IQueryable<Ticket>>().Setup(m => m.Expression).Returns(data.Expression);
            mockSet.As<IQueryable<Ticket>>().Setup(m => m.ElementType).Returns(data.ElementType);
            mockSet.As<IQueryable<Ticket>>().Setup(m => m.GetEnumerator()).Returns(data.GetEnumerator());

            _mockContext.Setup(m => m.Tickets).Returns(mockSet.Object);

            var repo = new TicketRepository(_mockContext.Object);
            var tickets1 = repo.ReadTicketsByText(s1).ToList();
            var tickets2 = repo.ReadTicketsByText(s2).ToList();
            var tickets3 = repo.ReadTicketsByText(s3).ToList();

            Assert.Empty(tickets1);
            Assert.NotEmpty(tickets2);
            Assert.Empty(tickets3);
        }

        [Theory]
        [InlineData(1)]
        [InlineData(2)]
        [InlineData(3)]
        public void ReadTicketByIdTest(int id)
        {
            Ticket t1 = new Ticket {TicketNumber = id, Text = "a"};

            var mockSet = new Mock<DbSet<Ticket>>();
            mockSet.Setup(x => x.Find(id)).Returns(t1);
            _mockContext.Setup(m => m.Tickets).Returns(mockSet.Object);

            var repo = new TicketRepository(_mockContext.Object);
            var ticket = repo.ReadTicket(id);

            Assert.NotNull(ticket);
            Assert.Equal(t1, ticket);
        }

        /**
         * Check if call is made on DbContext
         */
        [Fact]
        public void UpdateTicket_updates_a_ticket_via_context()
        {
            // Arrange
            var mockSet = new Mock<DbSet<Ticket>>();
            _mockContext.Setup(m => m.Tickets).Returns(mockSet.Object);
            Ticket t1 = new Ticket {TicketNumber = 1, Text = "a"};

            // Act
            _repo.UpdateTicket(t1);


            // Assert
            mockSet.Verify(m => m.Update(It.IsAny<Ticket>()), Times.Once());
            _mockContext.Verify(m => m.SaveChanges(), Times.Once());
        }

        /**
         * Check if call is made on DbContext
         */
        [Fact]
        public void DeleteTicket_deletes_a_ticket_via_context()
        {
            // Arrange
            var mockSet = new Mock<DbSet<Ticket>>();
            _mockContext.Setup(m => m.Tickets).Returns(mockSet.Object);
            int id = 1;

            // Act
            _repo.DeleteTicket(id);


            // Assert
            mockSet.Verify(m => m.Remove(It.IsAny<Ticket>()), Times.Once());
            _mockContext.Verify(m => m.SaveChanges(), Times.Once());
        }


        [Fact]
        public void CreateTicketResponse_saves_a_ticket_via_context()
        {
            // Arrange
            var mockSet = new Mock<DbSet<TicketResponse>>();
            _mockContext.Setup(m => m.TicketResponses).Returns(mockSet.Object);
            Ticket t1 = new Ticket {TicketNumber = 1, Text = "a"};
            TicketResponse tr1 = new TicketResponse
            {
                Date = DateTime.Now,
                Id = 1,
                IsClientResponse = true,
                Ticket = t1,
                Text = "a"
            };

            // Act
            _repo.CreateTicketResponse(tr1);


            // Assert
            mockSet.Verify(m => m.Add(It.IsAny<TicketResponse>()), Times.Once());
            _mockContext.Verify(m => m.SaveChanges(), Times.Once());
        }
        
        
        /**
         * 
         */
        [Fact]
        public void ReadTicketResponsesTest()
        {
            int expectedSize = 3;
            Ticket t1 = new Ticket {TicketNumber = 1, Text = "a"};
            Ticket t2 = new Ticket {TicketNumber = 2, Text = "a"};
            TicketResponse tr1 = new TicketResponse
            {
                Id = 1,
                Ticket = t1,
                Text = "a"
            };
            TicketResponse tr2 = new TicketResponse
            {
                Id = 1,
                Ticket = t1,
                Text = "a"
            };
            TicketResponse tr3 = new TicketResponse
            {
                Id = 1,
                Ticket = t1,
                Text = "a"
            };
            TicketResponse tr4 = new TicketResponse
            {
                Id = 1,
                Ticket = t2,
                Text = "a"
            };
            var responseList = new List<TicketResponse>{tr1,tr2,tr3,tr4}.AsQueryable();

            var mockSet = new Mock<DbSet<TicketResponse>>();
            mockSet.As<IQueryable<Ticket>>().Setup(m => m.Provider).Returns(responseList.Provider);
            mockSet.As<IQueryable<Ticket>>().Setup(m => m.Expression).Returns(responseList.Expression);
            mockSet.As<IQueryable<Ticket>>().Setup(m => m.ElementType).Returns(responseList.ElementType);
            mockSet.As<IQueryable<TicketResponse>>().Setup(m => m.GetEnumerator()).Returns(responseList.GetEnumerator());

            _mockContext.Setup(m => m.TicketResponses).Returns(mockSet.Object);

            var repo = new TicketRepository(_mockContext.Object);
            var responses = repo.ReadTicketResponsesOfTicket(t1.TicketNumber);

            Assert.Equal(expectedSize, responses.Count());
        }
    }
}