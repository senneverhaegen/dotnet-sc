using System;
using Microsoft.AspNetCore.Mvc;
using Moq;
using SC.BL;
using SC.BL.Domain;
using SC.UI.Web.MVC.Controllers.Api;
using Xunit;

namespace Tests
{
  public class TicketsControllerTests
  {
    private readonly Mock<ITicketManager> _mgrMock;
    private readonly TicketsController _controller;

    public TicketsControllerTests()
    {
      _mgrMock = new Mock<ITicketManager>();
      _controller = new TicketsController(_mgrMock.Object);
    }

    [Fact]
    public void Put_Return_NotFound()
    {
      //Arrange

      //Act
      var result = _controller.PutTicketStateToClosed(1);

      //Assert
      Assert.Equal(typeof(NotFoundResult),result.GetType());
    }

    [Fact]
    public void Put_Return_NoContent()
    {
      //Arrange
      //Arange
      String testText = "Hey Hoy";
      int accId = 1;
      Ticket t1 = new Ticket()
      {
        TicketNumber = 1,
        AccountId = accId,
        Text = testText,
        DateOpened = DateTime.Now,
        State = TicketState.Open
      };

      //Act

      _mgrMock.Setup(x => x.AddTicket(accId, testText)).Returns(t1);
      _mgrMock.Setup(x => x.GetTicket(1)).Returns(t1);
      var result = _controller.PutTicketStateToClosed(1);
      //Assert
      Assert.Equal(typeof(NoContentResult),result.GetType());

    }

  }
}