using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.AspNetCore.Mvc;
using Moq;
using SC.BL;
using SC.BL.Domain;
using SC.UI.Web.MVC.Controllers;
using SC.UI.Web.MVC.Models;
using Xunit;

namespace Tests
{
  public class TicketControllerTests
  {
    private readonly Mock<ITicketManager> _mockTicketManager;
    private readonly TicketController _tktCo;

    public TicketControllerTests()
    {
      _mockTicketManager = new Mock<ITicketManager>();
      _tktCo = new TicketController(_mockTicketManager.Object);
    }

    [Fact]
    public void Test_TktCo_Index()
    {
      //Arrange
      Ticket t1 = new Ticket()
      {
        TicketNumber = 1,
        AccountId = 1,
        Text = "problem",
        DateOpened = DateTime.Now,
        State = TicketState.Open
      };
      Ticket t2 = new Ticket()
      {
        TicketNumber = 2,
        AccountId = 2,
        Text = "problem",
        DateOpened = DateTime.Now,
        State = TicketState.Open
      };
      List<Ticket> expected = new List<Ticket>{t1,t2};
      _mockTicketManager.Setup(x => x.GetTickets()).Returns(expected);
      // Act
      var result = _tktCo.Index();

      // Assert
      var viewResult = Assert.IsType<ViewResult>(result);
      var model = Assert.IsAssignableFrom<IEnumerable<Ticket>>(
        viewResult.ViewData.Model);
      Assert.Equal(2, model.Count());
    }

    [Fact]
    public void Test_TktCo_Details()
    {

      //Arange
      String testText = "Hey Hoy";
      int accId = 1;
      Ticket t1 = new Ticket()
      {
        TicketNumber = 1,
        AccountId = accId,
        Text = testText,
        DateOpened = DateTime.Now,
        State = TicketState.Open
      };
      TicketResponse tk = new TicketResponse()
      {
        Id = 1,
        Date = DateTime.Now,
        IsClientResponse = false,
        Text = "Hey Hoy",
        Ticket = t1
      };


      // Act
      _mockTicketManager.Setup(x => x.AddTicket(accId,testText)).Returns(t1);
      _mockTicketManager.Setup(x => x.AddTicketResponse(accId, "Hey Hoy", false)).Returns(tk);
      _mockTicketManager.Setup(x => x.GetTicket(1)).Returns(t1);
      var result = _tktCo.Details(1) as ViewResult;
      var ticket = (Ticket) result.ViewData.Model;

      // Assert
      Assert.Equal(testText, ticket.Text);
    }

    [Fact]
    public void Test_TktCo_Edit_GET()
    {
      //Arrange
      //Arange
      String testText = "Hey Hoy";
      int accId = 1;
      Ticket t1 = new Ticket()
      {
        TicketNumber = 1,
        AccountId = accId,
        Text = testText,
        DateOpened = DateTime.Now,
        State = TicketState.Open
      };


      // Act
      _mockTicketManager.Setup(x => x.AddTicket(accId,testText)).Returns(t1);
      _mockTicketManager.Setup(x => x.GetTicket(1)).Returns(t1);

      // Assert
      var result = _tktCo.Edit(1) as ViewResult;
      var ticket = (Ticket) result.ViewData.Model;

      // Assert
      Assert.Equal(t1, ticket);

    }
    [Fact]
    public void Test_TktCo_Edit_POST_IsNotValid()
    {
      // Arrange
      String testText = "Hey Hoy";
      int accId = 1;
      Ticket t1 = new Ticket()
      {
        TicketNumber = 1,
        AccountId = accId,
        Text = testText,
        DateOpened = DateTime.Now,
        State = TicketState.Open
      };

      // Act
      _mockTicketManager.Setup(x => x.AddTicket(accId,testText)).Returns(t1);
      _mockTicketManager.Setup(x => x.GetTicket(1)).Returns(t1);
      Ticket t2 = new Ticket()
      {
        TicketNumber = 1,
        AccountId = accId,
        Text ="eu turpis egestas pretium aenean pharetra magna ac placerat vestibulum lectus mauris ultrices eros in cursus turpis massa tincidunt dui ut ornare lectus sit amet est placerat in egestas erat imperdiet sed euismod nisi porta lorem mollis aliquam ut porttitor leo a diam sollicitudin tempor id eu nisl nunc mi",
        DateOpened = DateTime.Now,
        State = TicketState.Open
      };
      _tktCo.ModelState.AddModelError("Text","TooLong");
      var result = _tktCo.Edit(1, t2);
      var viewResult = Assert.IsType<ViewResult>(result);
      var ticket = (Ticket) viewResult.ViewData.Model;

      // Assert
      Assert.Equal(t2,ticket);

    }

    [Fact]
    public void Test_TktCo_Edit_POST()
    {
      // Arrange
      String testText = "Hey Hoy";
      int accId = 1;
      Ticket t1 = new Ticket()
      {
        TicketNumber = 1,
        AccountId = accId,
        Text = testText,
        DateOpened = DateTime.Now,
        State = TicketState.Open
      };

      // Act
      _mockTicketManager.Setup(x => x.AddTicket(accId,testText)).Returns(t1);
      _mockTicketManager.Setup(x => x.GetTicket(1)).Returns(t1);
      Ticket t2 = new Ticket()
      {
        TicketNumber = 1,
        AccountId = accId,
        Text ="eu turpis egestas pretium",
        DateOpened = DateTime.Now,
        State = TicketState.Open
      };
      var result = _tktCo.Edit(1, t2);
      var redirectToActionResult = Assert.IsType<RedirectToActionResult>(result);

      // Assert
      Assert.Null(redirectToActionResult.ControllerName);
      Assert.Equal("Details", redirectToActionResult.ActionName);
    }


    [Fact]
    public void Test_TktCo_Create_GET()
    {
      // Act
      var result = _tktCo.Create() as ViewResult;

      // Assert
      Assert.NotNull(result);
    }

    [Fact]
    public void Test_TktCo_Create_POST()
    {
      //Arrange
      int accId = 1;
      String testText = "Hey Hoy";
      Ticket t1 = new Ticket()
      {
        TicketNumber = 1,
        AccountId = accId,
        Text = testText,
        DateOpened = DateTime.Now,
        State = TicketState.Open
      };

      CreateTicketViewModel cr = new CreateTicketViewModel
      {
        AccId = accId,
        Problem = testText
      };

      // Act
      _mockTicketManager.Setup(x => x.AddTicket(accId, testText)).Returns(t1);
      var result = _tktCo.Create(cr);
      var redirectToActionResult = Assert.IsType<RedirectToActionResult>(result);

      // Assert
      Assert.Null(redirectToActionResult.ControllerName);
      Assert.Equal("Details", redirectToActionResult.ActionName);

    }



    [Fact]
    public void Test_TktCo_Delete()
    {
      //Arrange
      int accId = 1;
      String testText = "Hey Hoy";
      Ticket t1 = new Ticket()
      {
        TicketNumber = 1,
        AccountId = accId,
        Text = testText,
        DateOpened = DateTime.Now,
        State = TicketState.Open
      };

      // Act
      _mockTicketManager.Setup(x => x.AddTicket(accId, testText)).Returns(t1);
      _mockTicketManager.Setup(x => x.GetTicket(1)).Returns(t1);
      var result = _tktCo.Delete(1);

      var viewResult = Assert.IsType<ViewResult>(result);
      var ticket = (Ticket) viewResult.ViewData.Model;
      // Assert
      Assert.Equal(t1,ticket);
    }

    [Fact]
    public void Test_TktCo_Delete_Confirmed()
    {
      //Arrange
      int accId = 1;
      String testText = "Hey Hoy";
      Ticket t1 = new Ticket()
      {
        TicketNumber = 1,
        AccountId = accId,
        Text = testText,
        DateOpened = DateTime.Now,
        State = TicketState.Open
      };

     // Act
      _mockTicketManager.Setup(x => x.AddTicket(accId, testText)).Returns(t1);
      var result = _tktCo.DeleteConfirmed(1);

      var redirectToActionResult = Assert.IsType<RedirectToActionResult>(result);
      // Assert
      Assert.Null(redirectToActionResult.ControllerName);
      Assert.Equal("Index", redirectToActionResult.ActionName);

    }
  }
}