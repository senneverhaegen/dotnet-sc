using System.Net;
using Microsoft.AspNetCore.Mvc;
using SC.BL;
using SC.BL.Domain;

namespace SC.UI.Web.MVC.Controllers.Api
{
    [ApiController]
    [Route("api/[controller]")]
    public class TicketsController : ControllerBase
    {
        private readonly ITicketManager _mgr;

        public TicketsController(ITicketManager mgr)
        {
            _mgr = mgr;
        }

        // PUT: api/Tickets/5/State/Closed
        [HttpPut("{id}/State/Closed")]
        public IActionResult PutTicketStateToClosed(int id)
        {
            Ticket ticket = _mgr.GetTicket(id);

            if (ticket == null)
                return NotFound();

            ticket.State = TicketState.Closed;
            _mgr.ChangeTicket(ticket);
            
            return NoContent();
        }

    }
}