using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.EntityFrameworkCore;
using SC.BL.Domain;

namespace SC.DAL.EF
{
    public class TicketRepository : ITicketRepository
    {
        private readonly SupportCenterDbContext _ctx;

        public TicketRepository()
        {
            _ctx = new SupportCenterDbContext();
        }

        // For tests
        public TicketRepository(SupportCenterDbContext ctx)
        {
           _ctx = ctx;
        }

        public IEnumerable<Ticket> ReadTickets()
        {
            IEnumerable<Ticket> tickets = _ctx.Tickets
                .Include(t => t.Responses) // eager-loading 'Responses'
                .AsEnumerable();
            return tickets;
        }

        public IEnumerable<Ticket> ReadTicketsByText(string text)
        {
            IEnumerable<Ticket> tickets = _ctx.Tickets
                .Where(t => string.Equals(t.Text, text, StringComparison.InvariantCultureIgnoreCase))
                .Include(t => t.Responses) // eager-loading 'Responses'
                .AsEnumerable();
            return tickets;
        }

        public Ticket CreateTicket(Ticket ticket)
        {
            _ctx.Tickets.Add(ticket);
            _ctx.SaveChanges();

            return ticket;
        }

        public Ticket ReadTicket(int ticketNumber)
        {
            //Ticket ticket = ctx.Tickets.Single(t => t.TicketNumber == ticketNumber);
            Ticket ticket = _ctx.Tickets.Find(ticketNumber);
            return ticket;
        }

        public void UpdateTicket(Ticket ticket)
        {
            _ctx.Tickets.Update(ticket);
            _ctx.SaveChanges();
        }

        public void DeleteTicket(int ticketNumber)
        {
            Ticket ticketToDelete = this.ReadTicket(ticketNumber);
            _ctx.Tickets.Remove(ticketToDelete);
            _ctx.SaveChanges();
        }

        public IEnumerable<TicketResponse> ReadTicketResponsesOfTicket(int ticketNumber)
        {
            IEnumerable<TicketResponse> responses = _ctx.TicketResponses
                .Where(response => response.Ticket.TicketNumber == ticketNumber)
                .AsEnumerable();

            return responses;

            /* Explicit-loading */
            //Ticket ticket = ctx.Tickets.Find(ticketNumber);
            //ctx.Entry(ticket).Collection(t => t.Responses).Load();
            //return ticket.Responses;
        }

        public TicketResponse CreateTicketResponse(TicketResponse response)
        {
            _ctx.TicketResponses.Add(response);
            _ctx.SaveChanges();

            return response;
        }
    }
}