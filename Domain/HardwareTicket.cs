﻿using System.ComponentModel.DataAnnotations;

namespace SC.BL.Domain
{
    public class HardwareTicket : Ticket
    {
        [RegularExpression(@"^[a-zA-Z0-9]+?$", ErrorMessage = "DeviceName does not match regex")]
        public string DeviceName { get; set; }
    }
}