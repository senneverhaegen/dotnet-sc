using System;
using System.Linq;
using System.Reflection;
using Castle.Core.Internal;
using Microsoft.EntityFrameworkCore.Extensions.Internal;
using SC.BL;
using SC.DAL;

namespace Reflection
{
  public class ProjectReflection
  {
    static void Main(string[] args)
    {
//      ReflectionOverall(typeof(ITicketManager));
//      ReflectionOverall(typeof(ITicketRepository));
      ReflectionOverall(typeof(ReflectionPurpose));
      ReflectionPurposeClass();
    }

    private static void ReflectionPurposeClass()
    {
      //Reflection Class Example set and invoke
      ReflectionPurpose instance = new ReflectionPurpose("Testing");
      Type type = instance.GetType();

      //Will only work on public variables
      //set values
      var fieldInfo = type.GetFields();
      foreach (var info in fieldInfo)
      {
        switch (info.Name)
        {
          case "intField":
            info.SetValue(instance, 99);
            Console.WriteLine("intField has now a value of '99'");
            break;
          case "stringField":
            info.SetValue(instance, "Testing StringField");
            Console.WriteLine("stringField has now a value of 'Testing StringField'");
            break;
          case "doubleField":
            info.SetValue(instance, 16.5);
            Console.WriteLine("doubleField has now a value of '16.5'");
            break;
        }
      }

      //read values
      Console.WriteLine("Reading setted public values");
      foreach (var info in fieldInfo)
      {
        Console.WriteLine("\t"+info.Name + ": " + info.GetValue(instance).ToString());
      }

      //Invoke Methods
      Console.WriteLine("\nInvoking methods");
//      var methods = type.GetMethods();
      //BindingFlags.DeclaredOnly -> gedeclareerd binnen classe
      //BindingFlags.NonPublic | BindingFlags.Public -> Private | Public methods
      var methods = type.GetMethods(BindingFlags.Instance|BindingFlags.NonPublic|BindingFlags.Public|BindingFlags.DeclaredOnly).FindAll(m=>m.Name.Contains("Something"));
      foreach (var method in methods)
      {
        if (method.GetParameters().Length == 1)
        {
          method.Invoke(instance, new object[]{"This is a testing Message"});
        }
        else if (method.GetParameters().Length > 1)
        {
          Console.WriteLine(method.Name +  " Requires more than 1 parameter and was not invoked.");
        }
        else
        {
          method.Invoke(instance, null);
        }
      }
    }

    private static void ReflectionOverall(Type ClassToType)
    {
      BindingFlags InstanceDeclared = BindingFlags.Instance | BindingFlags.DeclaredOnly;
      BindingFlags publicInstanceDeclared = BindingFlags.Public | InstanceDeclared;
      BindingFlags privateInstanceDeclared = BindingFlags.NonPublic | InstanceDeclared;
      BindingFlags pubPrivInstanceDeclared = BindingFlags.Public | privateInstanceDeclared;

      Console.WriteLine("\tReflectioning class " + ClassToType.FullName);
      var assembly = ClassToType.Assembly;
      String name = ClassToType.Name;
      Console.WriteLine("Assembly Name: " + assembly.GetName().Name);
      Console.WriteLine("Version: " + assembly.GetName().Version.ToString());
      Console.WriteLine(name + " Assembly Info" + assembly);
      Console.WriteLine("Type: " + name);


      var typeInfo = ClassToType.GetTypeInfo();

      Console.WriteLine("Has " + typeInfo.DeclaredMembers.Count().ToString() + " Members.");
      var members = ClassToType.GetMembers(pubPrivInstanceDeclared);
      foreach (var member in members)
      {
        Console.WriteLine("\tMember: " + member.Name);
      }

      Console.WriteLine("Has " + typeInfo.DeclaredProperties.Count().ToString() + " properties.");
      var properties = ClassToType.GetProperties(pubPrivInstanceDeclared);
      foreach (var property in properties)
      {
        Console.WriteLine("\tProperty: " + property.Name + " PropertyType: " + property.PropertyType);
      }

      Console.WriteLine("Has " + typeInfo.DeclaredFields.Count().ToString() + " fields.");
      var publicFields = ClassToType.GetFields(publicInstanceDeclared);
      foreach (var field in publicFields)
      {
        Console.WriteLine("\tPublic Field: " + field.Name);
      }
      var privateFields = ClassToType.GetFields(privateInstanceDeclared);
      foreach (var field in privateFields)
      {
        Console.WriteLine("\tPrivate Field: " + field.Name);
      }

      Console.WriteLine("Has " + typeInfo.DeclaredMethods.Count().ToString() + " methods.");
      var publicMethods = ClassToType.GetMethods(publicInstanceDeclared);
      foreach (var method in publicMethods)
      {
        Console.WriteLine("\tPublic Methods: " + method.Name);
        Console.WriteLine("\t\tReturnType: " + method.ReturnType);
      }
      var privateMethods = ClassToType.GetMethods(privateInstanceDeclared);
      foreach (var method in privateMethods)
      {
        Console.WriteLine("\tPrivate Methods: " + method.Name);
        Console.WriteLine("\t\tReturnType: " + method.ReturnType);
      }
    }
  }
}