using System;

namespace Reflection
{
  public class ReflectionPurpose
  {
    private string Name;

    public ReflectionPurpose(string name)
    {
      Name = name;
    }

    private int intProperty { get; set; }
    public string stringProperty { get; set; }
    private double doubleProperty { get; set; }
    public int intField;
    public string stringField;
    private double doubleField;



    private void SaySomething()
    {
      Console.WriteLine("Saying: Something");
    }

    public void SaySomething2()
    {
      Console.WriteLine("Saying: Something2");
    }

    public void SaySomething3()
    {
      Console.WriteLine("Saying: Something3");
    }

    public void SaySomething4(string message)
    {
      Console.WriteLine("Saying: "+message);
    }

    private void SaySomething5(string message, int nm)
    {
      Console.WriteLine("Saying: "+message+" "+nm);
    }

  }
}